package sfg.gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {
	private Controller controller;
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("SFG.fxml"));
		Parent root = fxmlLoader.load();
		primaryStage.setTitle("SFG");
		Scene scene = new Scene(root, 800, 600);
		//		primaryStage.setResizable(false);
		primaryStage.getIcons().add(new Image(Main.class.getResourceAsStream("/sfg/gui/assets/icon.png")));
		primaryStage.setScene(scene);
		primaryStage.show();
		controller = fxmlLoader.getController();
	}

	@Override
	public void stop() {
		controller.freeResources();
	}
}
