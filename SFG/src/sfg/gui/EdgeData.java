package sfg.gui;

public class EdgeData {
	private String source;
	private String dist;
	private double gain;

	public EdgeData(String src, String dist, double gain) {
		this.source = src;
		this.dist = dist;
		this.gain = gain;
	}

	public String getSource() {
		return source;
	}

	public String getDist() {
		return dist;
	}

	public double getGain() {
		return gain;
	}

}
