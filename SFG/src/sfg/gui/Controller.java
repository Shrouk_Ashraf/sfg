package sfg.gui;
/**
 * Sample Skeleton for 'SFG.fxml' Controller Class
 */

import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.IdAlreadyInUseException;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.MultiGraph;
import org.graphstream.ui.swingViewer.ViewPanel;
import org.graphstream.ui.view.Viewer;
import org.graphstream.ui.view.Viewer.CloseFramePolicy;

import javafx.beans.binding.BooleanBinding;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingNode;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.util.Callback;
import sfg.algo.Path;
import sfg.algo.Result;
import sfg.algo.SFG;
import sfg.algo.WeightedPath;

public class Controller {
	private SFG sfg;

	private Collection<String> nodeIds;
	@FXML // ResourceBundle that was given to the FXMLLoader
	private ResourceBundle resources;

	@FXML // URL location of the FXML file that was given to the FXMLLoader
	private URL location;

	//	@FXML
	//	private Canvas graphCanvas;

	@FXML
	private AnchorPane canvasPane;

	@FXML
	private TextFlow outputTextBox;

	private Graph graph;

	private static final String GAIN_ATTRIBUTE = "gain";

	private static final String UI_LABEL_ATTRIBUTE = "ui.label";

	private static final String HIGHLIGHTED_NODE_CLASS = "important";

	private static final String HIGHLIGHTED_EDGE_CLASS = "highlighted";

	private static final String NORMAL_EDGE_CLASS = "black";


	private static final String SEPARATOR = ", ";

	private static final String UI_CLASS_ATTRIBUTE = "ui.class";

	private static final String EDGE_ID_SEPARATOR = "->";

	private Viewer viewer;

	public Controller() {
		graph = new MultiGraph("SFG Graph");
		initGraph();
		nodeIds = new ArrayList<>();
		sfg = new SFG();
	}

	@FXML // This method is called by the FXMLLoader when initialization is complete
	void initialize() {
		initCanvas();
	}

	private void initGraph() {
		this.graph.addAttribute("ui.stylesheet", "url('" + this
				.getClass().getClassLoader().getResource("sfg/gui/graph.css") + "')");
		this.graph.addAttribute("ui.quality");
		this.graph.addAttribute("ui.antialias");
	}

	private void initCanvas() {
		System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
		viewer = new Viewer(graph, Viewer.ThreadingModel.GRAPH_IN_GUI_THREAD);
		ViewPanel viewPanel = viewer.addDefaultView(false);
		viewer.enableAutoLayout();
		viewer.setCloseFramePolicy(CloseFramePolicy.EXIT);
		SwingNode node = new SwingNode();
		node.setContent(viewPanel);
		canvasPane.getChildren().add(node);
		AnchorPane.setLeftAnchor(node, 0.0);
		AnchorPane.setRightAnchor(node, 0.0);
		AnchorPane.setTopAnchor(node, 0.0);
		AnchorPane.setBottomAnchor(node, 0.0);
		canvasPane.toBack();
		node.toFront();
	}

	private void highlightPath(String path) {
		clearHighlighting();
		String[] edges = path.split(SEPARATOR);
		for (String edgeId : edges) {
			Edge edge = graph.getEdge(edgeId);
			edge.getNode0().addAttribute(UI_CLASS_ATTRIBUTE, HIGHLIGHTED_NODE_CLASS);
			edge.getNode1().addAttribute(UI_CLASS_ATTRIBUTE, HIGHLIGHTED_NODE_CLASS);
			edge.addAttribute(UI_CLASS_ATTRIBUTE, HIGHLIGHTED_EDGE_CLASS);
		}
	}

	@FXML
	private void addNode(ActionEvent event) {
		TextInputDialog inputDialog = new TextInputDialog();
		inputDialog.setHeaderText("Add a New Node");
		inputDialog.setContentText("Enter the id of the node:");
		Optional<String> input = inputDialog.showAndWait();
		if (input.isPresent()) {
			try {
				Node node = graph.addNode(input.get());
				node.addAttribute(UI_LABEL_ATTRIBUTE, input.get());
				nodeIds.add(input.get());
			} catch (IdAlreadyInUseException ex) {
				Alert alert = new Alert(AlertType.ERROR, "A Node With The Same Name"
						+ " Already Exists in The Graph", ButtonType.OK);
				alert.setHeaderText("Duplicate Node");
				alert.show();
			}
		}
	}

	@FXML
	private void addEdge(ActionEvent event) {
		Dialog<EdgeData> inputDialog = new Dialog<>();
		inputDialog.setHeaderText("Add a New Edge");
		Label sourceLabel = new Label("Source Node: ");
		ObservableList<String> nodeList = FXCollections.observableArrayList(nodeIds);
		ChoiceBox<String> srcChoiceBox = new ChoiceBox<>(nodeList);
		Label destLabel = new Label("Destination Node: ");
		ChoiceBox<String> destChoiceBox = new ChoiceBox<>(nodeList);
		Label gainLabel = new Label("Gain: ");
		TextField gainTextField = new TextField();
		GridPane gridPane = new GridPane();
		gridPane.add(sourceLabel, 0, 0);
		gridPane.add(srcChoiceBox, 1, 0);
		gridPane.add(destLabel, 0, 1);
		gridPane.add(destChoiceBox, 1, 1);
		gridPane.add(gainLabel, 0, 2);
		gridPane.add(gainTextField, 1, 2);
		ButtonType okButton = new ButtonType("OK", ButtonData.OK_DONE);
		ButtonType cancelButton = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
		inputDialog.getDialogPane().setContent(gridPane);
		inputDialog.getDialogPane().getButtonTypes().add(okButton);
		inputDialog.getDialogPane().getButtonTypes().add(cancelButton);
		BooleanBinding booleanBinding = new BooleanBinding() {
			{
				super.bind(gainTextField.textProperty(), srcChoiceBox.valueProperty(),
						destChoiceBox.valueProperty());
			}
			@Override
			protected boolean computeValue() {
				return (srcChoiceBox.getValue() == null)
						|| (destChoiceBox.getValue() == null)
						|| !isValidDouble(gainTextField.getText());
			}
		};
		Button okButtonNode = (Button) inputDialog.getDialogPane().lookupButton(okButton);
		okButtonNode.disableProperty().bind(booleanBinding);
		inputDialog.setResultConverter(new Callback<ButtonType, EdgeData>() {

			@Override
			public EdgeData call(ButtonType param) {
				if (!param.equals(okButton)) {
					return null;
				}
				return new EdgeData(srcChoiceBox.getValue(),
						destChoiceBox.getValue(), Double.parseDouble(gainTextField.getText()));
			}
		});
		Optional<EdgeData> data = inputDialog.showAndWait();
		if (data.isPresent()) {
			EdgeData edgeData = data.get();
			try {
				Edge edge = graph.addEdge(edgeData.getSource()
						+ EDGE_ID_SEPARATOR + edgeData.getDist(), edgeData.getSource(),
						edgeData.getDist(), true);
				edge.addAttribute(GAIN_ATTRIBUTE, edgeData.getGain());
				edge.addAttribute(UI_LABEL_ATTRIBUTE, edgeData.getGain());
			} catch (IdAlreadyInUseException ex) {
				Alert alert = new Alert(AlertType.ERROR, "An Edge With The Same Source"
						+ " and Destination Nodes Already Exists in The Graph", ButtonType.OK);
				alert.setHeaderText("Duplicate Edge");
				alert.show();
			}
		}
	}

	@FXML
	private void solve(ActionEvent event) {
		Dialog<Pair<String, String>> inputDialog = new Dialog<>();
		inputDialog.setHeaderText("Solve");
		Label sourceLabel = new Label("Source Node: ");
		ObservableList<String> nodeList = FXCollections.observableArrayList(nodeIds);
		ChoiceBox<String> srcChoiceBox = new ChoiceBox<>(nodeList);
		Label destLabel = new Label("Destination Node: ");
		ChoiceBox<String> sinkChoiceBox = new ChoiceBox<>(nodeList);
		GridPane gridPane = new GridPane();
		gridPane.add(sourceLabel, 0, 0);
		gridPane.add(srcChoiceBox, 1, 0);
		gridPane.add(destLabel, 0, 1);
		gridPane.add(sinkChoiceBox, 1, 1);
		ButtonType okButton = new ButtonType("OK", ButtonData.OK_DONE);
		ButtonType cancelButton = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
		inputDialog.getDialogPane().setContent(gridPane);
		inputDialog.getDialogPane().getButtonTypes().add(okButton);
		inputDialog.getDialogPane().getButtonTypes().add(cancelButton);
		inputDialog.setResultConverter(new Callback<ButtonType, Pair<String, String>>() {

			@Override
			public Pair<String, String> call(ButtonType param) {
				if (!param.equals(okButton)) {
					return null;
				}
				outputTextBox.getChildren().clear();
				return new Pair<>(srcChoiceBox.getValue(),
						sinkChoiceBox.getValue());
			}
		});
		BooleanBinding booleanBinding = new BooleanBinding() {
			{
				super.bind(srcChoiceBox.valueProperty(),
						sinkChoiceBox.valueProperty());
			}
			@Override
			protected boolean computeValue() {
				return (srcChoiceBox.getValue() == null)
						|| (sinkChoiceBox.getValue() == null);
			}
		};
		Button okButtonNode = (Button) inputDialog.getDialogPane().lookupButton(okButton);
		okButtonNode.disableProperty().bind(booleanBinding);
		Optional<Pair<String, String>> data = inputDialog.showAndWait();
		if (data.isPresent()) {
			Pair<String, String> sourceSinkData = data.get();
			Result result = sfg.performMasonFormula((MultiGraph) graph,
					sourceSinkData.getFirst(), sourceSinkData.getSecond());
			addOutput(result, sourceSinkData);
		}
	}

	private Hyperlink createHyperLink(String text) {
		Hyperlink hyperlink = new Hyperlink(text);
		System.out.println(text);
		hyperlink.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				highlightPath(hyperlink.getText());
			}
		});
		return hyperlink;
	}

	@FXML
	private void clear(ActionEvent event) {
		graph.clear();
		initGraph();
		nodeIds.clear();
		outputTextBox.getChildren().clear();
	}

	private boolean isValidDouble(String gain) {
		try {
			Double.parseDouble(gain);
			return true;
		} catch (NumberFormatException ex) {
			return false;
		}
	}

	@FXML
	private void clearHighlighting() {
		for (Node node : graph.getEachNode()) {
			node.removeAttribute(UI_CLASS_ATTRIBUTE);
		}
		for (Edge edge : graph.getEachEdge()) {
			edge.setAttribute(UI_CLASS_ATTRIBUTE, NORMAL_EDGE_CLASS);
		}
	}

	@FXML
	private void removeNode() {
		TextInputDialog inputDialog = new TextInputDialog();
		inputDialog.setHeaderText("Remove Node");
		inputDialog.setContentText("Enter the id of the node:");
		Optional<String> input = inputDialog.showAndWait();
		if (input.isPresent()) {
			Node node = graph.getNode(input.get());
			if (node == null) {
				Alert alert = new Alert(AlertType.ERROR, "The Specified Node Does Not Exist"
						+ " in The Graph", ButtonType.OK);
				alert.setHeaderText("Node Not Found");
				alert.show();
			} else {
				nodeIds.remove(input.get());
				graph.removeNode(input.get());
			}
		}
	}

	@FXML
	private void removeEdge() {
		Dialog<Pair<String, String>> inputDialog = new Dialog<>();
		inputDialog.setHeaderText("Remove Edge");
		Label sourceLabel = new Label("Source Node: ");
		ObservableList<String> nodeList = FXCollections.observableArrayList(nodeIds);
		ChoiceBox<String> srcChoiceBox = new ChoiceBox<>(nodeList);
		Label destLabel = new Label("Destination Node: ");
		ChoiceBox<String> destChoiceBox = new ChoiceBox<>(nodeList);
		GridPane gridPane = new GridPane();
		gridPane.add(sourceLabel, 0, 0);
		gridPane.add(srcChoiceBox, 1, 0);
		gridPane.add(destLabel, 0, 1);
		gridPane.add(destChoiceBox, 1, 1);
		ButtonType okButton = new ButtonType("OK", ButtonData.OK_DONE);
		ButtonType cancelButton = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
		inputDialog.getDialogPane().setContent(gridPane);
		inputDialog.getDialogPane().getButtonTypes().add(okButton);
		inputDialog.getDialogPane().getButtonTypes().add(cancelButton);
		inputDialog.setResultConverter(new Callback<ButtonType, Pair<String, String>>() {

			@Override
			public Pair<String, String> call(ButtonType param) {
				if (!param.equals(okButton)) {
					return null;
				}
				outputTextBox.getChildren().clear();
				return new Pair<>(srcChoiceBox.getValue(),
						destChoiceBox.getValue());
			}
		});
		BooleanBinding booleanBinding = new BooleanBinding() {
			{
				super.bind(srcChoiceBox.valueProperty(),
						destChoiceBox.valueProperty());
			}
			@Override
			protected boolean computeValue() {
				return (srcChoiceBox.getValue() == null)
						|| (destChoiceBox.getValue() == null);
			}
		};
		Button okButtonNode = (Button) inputDialog.getDialogPane().lookupButton(okButton);
		okButtonNode.disableProperty().bind(booleanBinding);
		Optional<Pair<String, String>> data = inputDialog.showAndWait();
		if (data.isPresent()) {
			Pair<String, String> edgeData = data.get();
			String id = edgeData.getFirst() + EDGE_ID_SEPARATOR + edgeData.getSecond();
			Edge edge = graph.getEdge(id);
			if (edge == null) {
				Alert alert = new Alert(AlertType.ERROR, "The Specified Edge Does Not Exist"
						+ " in The Graph", ButtonType.OK);
				alert.setHeaderText("Edge Not Found");
				alert.show();
			} else {
				graph.removeEdge(id);
			}
		}
	}

	@FXML
	private void modifyEdge() {
		Dialog<EdgeData> inputDialog = new Dialog<>();
		inputDialog.setHeaderText("Modify an Existing Edge");
		Label sourceLabel = new Label("Source Node: ");
		ObservableList<String> nodeList = FXCollections.observableArrayList(nodeIds);
		ChoiceBox<String> srcChoiceBox = new ChoiceBox<>(nodeList);
		Label destLabel = new Label("Destination Node: ");
		ChoiceBox<String> destChoiceBox = new ChoiceBox<>(nodeList);
		Label gainLabel = new Label("New Gain: ");
		TextField gainTextField = new TextField();
		GridPane gridPane = new GridPane();
		gridPane.add(sourceLabel, 0, 0);
		gridPane.add(srcChoiceBox, 1, 0);
		gridPane.add(destLabel, 0, 1);
		gridPane.add(destChoiceBox, 1, 1);
		gridPane.add(gainLabel, 0, 2);
		gridPane.add(gainTextField, 1, 2);
		ButtonType okButton = new ButtonType("OK", ButtonData.OK_DONE);
		ButtonType cancelButton = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
		inputDialog.getDialogPane().setContent(gridPane);
		inputDialog.getDialogPane().getButtonTypes().add(okButton);
		inputDialog.getDialogPane().getButtonTypes().add(cancelButton);
		BooleanBinding booleanBinding = new BooleanBinding() {
			{
				super.bind(gainTextField.textProperty(), srcChoiceBox.valueProperty(),
						destChoiceBox.valueProperty());
			}
			@Override
			protected boolean computeValue() {
				return (srcChoiceBox.getValue() == null)
						|| (destChoiceBox.getValue() == null)
						|| !isValidDouble(gainTextField.getText());
			}
		};
		Button okButtonNode = (Button) inputDialog.getDialogPane().lookupButton(okButton);
		okButtonNode.disableProperty().bind(booleanBinding);
		inputDialog.setResultConverter(new Callback<ButtonType, EdgeData>() {

			@Override
			public EdgeData call(ButtonType param) {
				if (!param.equals(okButton)) {
					return null;
				}
				return new EdgeData(srcChoiceBox.getValue(),
						destChoiceBox.getValue(), Double.parseDouble(gainTextField.getText()));
			}
		});
		Optional<EdgeData> data = inputDialog.showAndWait();
		if (data.isPresent()) {
			EdgeData edgeData = data.get();
			Edge edge = graph.getEdge(edgeData.getSource()
					+ EDGE_ID_SEPARATOR + edgeData.getDist());
			if (edge == null) {
				Alert alert = new Alert(AlertType.ERROR, "The Specified Edge Does Not Exist"
						+ " in The Graph", ButtonType.OK);
				alert.setHeaderText("Edge Not Found");
				alert.show();
			} else {
				edge.changeAttribute(GAIN_ATTRIBUTE, edgeData.getGain());
				edge.changeAttribute(UI_LABEL_ATTRIBUTE, edgeData.getGain());
			}
		}
	}

	private void addOutput(Result result, Pair<String, String> sourceSinkData) {
		final int largeFontSize = 20;
		Text text = new Text("Solution:\n");
		text.setFont(Font.font(largeFontSize));
		outputTextBox.getChildren().add(text);
		outputTextBox.getChildren().add(new Text("Overall Transfer Function("
				+ sourceSinkData.getSecond() + "/"
				+ sourceSinkData.getFirst()
				+ "): " + result.getValueOfTF() + "\n"));
		outputTextBox.getChildren().add(getSeparator());
		text = new Text("Forward Paths:\n");
		text.setFont(Font.font(largeFontSize));
		outputTextBox.getChildren().add(text);
		int i = 0;
		for (WeightedPath path : result.getFwPaths()) {
			addWeightedPath("Forward Path", path, ++i);
		}
		outputTextBox.getChildren().add(getSeparator());
		text = new Text("Loops:\n");
		text.setFont(Font.font(largeFontSize));
		outputTextBox.getChildren().add(text);
		i = 0;
		for (WeightedPath path : result.getLoops()) {
			addWeightedPath("Loop", path, ++i);
		}
		i = 0;
		outputTextBox.getChildren().add(getSeparator());
		text = new Text("Deltas:\n");
		text.setFont(Font.font(largeFontSize));
		outputTextBox.getChildren().add(text);
		StringBuilder sBuilder = new StringBuilder();
		for (double delta : result.getDeltas()) {
			if (i == 0) {
				sBuilder.append("Delta: ");
			} else {
				sBuilder.append("Delta #");
				sBuilder.append(i);
				sBuilder.append(": ");
			}
			i++;
			sBuilder.append(delta);
			sBuilder.append("\n");
		}
		outputTextBox.getChildren().add(new Text(sBuilder.toString()));
		outputTextBox.getChildren().add(getSeparator());
		text = new Text("Non Touching Loops:\n");
		text.setFont(Font.font(largeFontSize));
		outputTextBox.getChildren().add(text);
		i = 0;
		int prevSize = 0;
		for (List<WeightedPath> arr : result.getNontouchloops()) {
			if (prevSize != arr.size()) {
				i = 0;
			}
			prevSize = arr.size();
			if (arr.size() > 1) {
				outputTextBox.getChildren().add(new Text("System #" + (++i) + " of Non Touching Loops of Length("
						+ arr.size() + "): "));
				for (Path path : arr) {
					addPath(path);
					outputTextBox.getChildren().add(new Text("\t"));
				}
				outputTextBox.getChildren().add(new Text("\n"));
			}
		}
	}

	private void addWeightedPath(String pathType, WeightedPath path, int pathIndex) {
		outputTextBox.getChildren().add(new Text(pathType + " #" + pathIndex + ":"));
		outputTextBox.getChildren().add(createHyperLink(path.toString()));
		outputTextBox.getChildren().add(new Text("\tGain: " + path.getGain() + "\n"));
	}

	private Text getSeparator() {
		StringBuilder sb = new StringBuilder();
		int len = 100;
		for (int i = 0 ; i < len ; i++) {
			sb.append("-");
		}
		sb.append("\n");
		return new Text(sb.toString());
	}

	private void addPath(Path path) {
		outputTextBox.getChildren().add(createHyperLink(path.toString()));
	}

	void freeResources() {
		viewer.close();
	}
}
