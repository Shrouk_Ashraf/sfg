package sfg.algo;

import java.util.ArrayList;
import java.util.HashSet;

import org.graphstream.graph.implementations.MultiGraph;
public class Test {

	public static void main(String args[]) {
		MultiGraph graph = new MultiGraph("Tutorial 1");
		SFG sfg = new SFG();
		graph.addNode("A");
		graph.addNode("B");
		graph.addNode("C");
		graph.addNode("D");
		graph.addNode("E");
		
		
		
		graph.addEdge("AB", "A", "B",true);
		graph.addEdge("BC", "B", "C",true);
		graph.addEdge("CD", "C", "D",true);
		graph.addEdge("DE", "D", "E",true);
		graph.addEdge("BD", "B", "D",true);
		graph.addEdge("BE", "B", "E",true);
		graph.addEdge("DC", "D", "C",true);
		graph.addEdge("CB", "C", "B",true);
		graph.addEdge("DD", "D", "D",true);
		
		double i=1;
		graph.getEdge("AB").addAttribute("gain", i*2);
		graph.getEdge("BC").addAttribute("gain", i*3);
		graph.getEdge("CD").addAttribute("gain", i*4);
		graph.getEdge("DE").addAttribute("gain", i*5);
		graph.getEdge("BD").addAttribute("gain", i*6);
		graph.getEdge("BE").addAttribute("gain", i*7);
		graph.getEdge("CB").addAttribute("gain", i*8);
		graph.getEdge("DC").addAttribute("gain", i*9);
		graph.getEdge("DD").addAttribute("gain", i*10);

		Result test = sfg.performMasonFormula(graph,"A","E");
		
		System.out.println("start");
		ArrayList<WeightedPath> paths = new ArrayList<>(test.getFwPaths());
		for (WeightedPath path : paths) {
			System.out.println(path.toString());
			System.out.println(path.getNodes().toString());
		}
		
		System.out.println("TF"+ test.getValueOfTF());
		for (double delta : test.getDeltas()) {
			System.out.println(delta);
		}
		System.out.println("loops");
		ArrayList<WeightedPath> loops = new ArrayList<>(test.getLoops());
		
			System.out.println(loops.get(0).toString());
	
		System.out.println(paths.get(2).intersects(loops.get(0)));
		
		
	}
}