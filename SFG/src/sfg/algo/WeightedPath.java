package sfg.algo;
import java.util.ArrayList;

import java.util.Collection;
import java.util.LinkedHashSet;

import org.graphstream.graph.Edge;

public class WeightedPath extends Path {
	private double gain;
	private String GAIN_ATTRIBUTE = "gain";

	public WeightedPath() {
		super();
		gain = 1;
	}

	public WeightedPath(WeightedPath path) {
		// System.out.println(path.getPath().size());
		this.path = new ArrayList<>(path.getPath());
		this.gain = path.getGain();
		this.pathNodes = new LinkedHashSet<>(path.getNodes());

	}

	public WeightedPath(ArrayList<Edge> path, double gain) {

		this.path = new ArrayList<>(path);
		this.gain = gain;
		for (Edge e : path) {
			pathNodes.add(e.getNode0());
		}
	}

	@Override
	public void addEdge(Edge edge) {
		super.addEdge(edge);
		double edgeGain = (Double) edge.getAttribute(GAIN_ATTRIBUTE);
		gain *= edgeGain;
	}

	@Override
	public void addEdges(Collection<Edge> edges) {
		super.addEdges(edges);
		for (Edge edge : edges) {
			double edgeGain = edge.getAttribute(GAIN_ATTRIBUTE);
			gain *= edgeGain;
		}
	}

	@Override
	public boolean removeEdge(Edge edge) {
		boolean flag = super.removeEdge(edge);
		double edgeGain = edge.getAttribute(GAIN_ATTRIBUTE);
		gain /= edgeGain;
		return flag;
	}

	public double getGain() {
		return gain;
	}

	public WeightedPath sublist(int indexS, int indexE) {
		ArrayList<Edge> temp;
		if (indexS >= 0) {
			temp = new ArrayList<Edge>(path.subList(indexS, indexE));
		} else {
			// self loop handling
			temp = new ArrayList<Edge>();
		}
		double loopGain = 1;
		for (Edge edge : temp) {
			loopGain *= (Double) edge.getAttribute(GAIN_ATTRIBUTE);
		}
		WeightedPath subList = new WeightedPath(temp, loopGain);
		return subList;
	}
}
