package sfg.algo;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.MultiGraph;

public class Path {
	protected ArrayList<Edge> path;
	protected LinkedHashSet<Node> pathNodes;
	private static final String SEPARATOR = ", ";
	public Path() {
		path = new ArrayList<>();
		pathNodes = new LinkedHashSet<>();

	}

	public Path(ArrayList<Edge> path) {
		this.path = new ArrayList<>(path);
		for (Edge e : path) {
			pathNodes.add(e.getNode0());
		}
	}

	public ArrayList<Edge> getPath() {
		return path;
	}

	public LinkedHashSet<Node> getNodes() {
		return pathNodes;

	}

	public void addEdge(Edge edge) {
		path.add(edge);
		pathNodes.add(edge.getNode0());
		pathNodes.add(edge.getNode1());
	}

	public void addEdges(Collection<Edge> edges) {
		path.addAll(edges);
	}

	public boolean removeEdge(Edge edge) {
		
		pathNodes.remove(edge.getNode1());
		return path.remove(edge);
	}

	boolean containsEdge(Edge edge) {
		return path.contains(edge);
	}

	public Iterator<Edge> getIterator() {
		return path.iterator();
	}

	public int size() {
		return path.size();
	}

	public ArrayList<Node> detectNodes() {

		ArrayList<Node> temp = new ArrayList<>();
		for (Edge e : path) {
			temp.add(e.getNode0());
		}
		return temp;

	}
	public boolean intersects(Path otherPath) {
		System.out.println("Zew "+ path.toString());
		Collection<Node> temp = new LinkedHashSet<>(pathNodes);
		System.out.println("path nodes");
		for (Node node : temp){
		System.out.println(node.getId());
		}
		temp.retainAll(otherPath.getNodes());
		System.out.println("intersection nodes");
		for (Node node : temp){
		System.out.println(node.getId());
		}
		return !temp.isEmpty();
	}

	public Path  subList(int indexS, int indexE) {
		ArrayList<Edge> sublist = new ArrayList<> (path.subList(indexS, indexE));

		Path duplicate = new Path (sublist);
		duplicate.pathNodes.clear();
		for (Edge e : sublist) {
			pathNodes.add(e.getNode0());
		}
		return duplicate;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((path == null) ? 0 : new HashSet<>(path).hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		//System.out.println("here");
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Path other = (Path) obj;
		if (path == null) {
			if (other.path != null)
				return false;
		} else {
			HashSet<Edge> pathSet = new HashSet<>(path);
			HashSet<Edge> otherSet = new HashSet<>(other.path);
			return pathSet.equals(otherSet);
		}
		return false;
	}

	@Override
	public String toString() {
		if (pathNodes.isEmpty()) {
			return "";
		}
		StringBuilder sb = new StringBuilder();
		for (Edge edge : path) {
			sb.append(edge.getId());
			sb.append(SEPARATOR);
		}
		return sb.substring(0, sb.length() - SEPARATOR.length()).toString();
	}
	public static void main(String args[]) {
		
		MultiGraph graph = new MultiGraph("Tutorial 1");
		graph.addNode("A");
		graph.addNode("B");
		graph.addNode("C");
		graph.addEdge("AB", "A", "B",true);
		graph.addEdge("BC", "B", "C",true);
	    Path path = new Path();
	    path.addEdge(graph.getEdge("AB"));
	    path.addEdge(graph.getEdge("BC"));
	    System.out.println(path.getNodes().size());
	    path.removeEdge(graph.getEdge("BC"));
	    System.out.println(path.getNodes().size());
	    
	}
}
