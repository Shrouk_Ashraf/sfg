package sfg.algo;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Result {

	private HashSet<WeightedPath> fwPaths;
	private HashSet<WeightedPath> loops;
	private List<Double> deltas;
	private List<List<List<Integer>>> nontouchloops;
	private double valueOfTF;

	public Result() {
		setFwPaths(new HashSet<WeightedPath>());
		setLoops(new HashSet<WeightedPath>());
		setNontouchloops(new ArrayList<>());
		setDeltas(new ArrayList<>());
	}

	public HashSet<WeightedPath> getFwPaths() {
		return fwPaths;
	}

	void setFwPaths(HashSet<WeightedPath> fwPaths) {
		this.fwPaths = fwPaths;
	}

	public HashSet<WeightedPath> getLoops() {
		return loops;
	}

	void setLoops(HashSet<WeightedPath> loops) {
		this.loops = loops;
	}

	List<List<List<Integer>>> getNontouchIndexes() {
		return nontouchloops;
	}

	public List<List<WeightedPath>> getNontouchloops() {
		List<WeightedPath> temp = new ArrayList<>(loops);
		List<List<WeightedPath>> nontouchLoops = new ArrayList<>();
		for (List<List<Integer>> ni : nontouchloops) {
			for (List<Integer> combPath : ni) {
				List<WeightedPath> loop = new ArrayList<>();
				for (int index : combPath) {
					loop.add(temp.get(index));

				}
				nontouchLoops.add(loop);
			}

		}

		return nontouchLoops;
	}

	void setNontouchloops(List<List<List<Integer>>> nontouchloops) {
		this.nontouchloops = nontouchloops;
	}

	public List<Double> getDeltas() {
		return deltas;
	}

	void setDeltas(List<Double> deltas) {
		this.deltas = deltas;
	}

	public double getValueOfTF() {
		return valueOfTF;
	}

	void setValueOfTF(double valueOfTF) {
		this.valueOfTF = valueOfTF;
	}

}
