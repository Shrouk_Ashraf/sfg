
package sfg.algo;

import java.util.ArrayList;
import java.util.List;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.MultiGraph;

public class SFG {

	private List<Boolean> visited;
	private double delta;
	private Result masonResult;

	public SFG() {
		visited = new ArrayList<>();
		delta = 0;
	}

	private void calculatePathsandLoops(Graph graph, String srcID, String sinkID) {
		visited.clear();
		delta = 0;
		for (Node n : graph.getEachNode()) {
			visited.add(false);
		}
		WeightedPath currentPath = new WeightedPath();
	
		detectPaths(graph, visited, graph.getNode(srcID), graph.getNode(sinkID), currentPath);
		detectAllLoops(graph, visited, currentPath);
	}

	private void detectPaths(Graph graph, List<Boolean> visited, Node current, Node des, WeightedPath path) {
		if (current == des) {
			masonResult.getFwPaths().add(new WeightedPath(path));

			return;
		} else {
			visited.set(current.getIndex(), true);
			for (Edge e : current.getEachLeavingEdge()) {
				Node two = e.getNode1();
				if (!visited.get(two.getIndex())) {
					path.addEdge(e);
					detectPaths(graph, visited, two, des, path);
					path.removeEdge(e);
				}
			}
			visited.set(current.getIndex(), false);
		}
	}

	private void detectAllLoops(Graph graph, List<Boolean> visited, WeightedPath path) {

		for (Node n : graph.getEachNode()) {
			detectLoop(graph, visited, n, n, path);
		}
	}

	private void detectLoop(Graph graph, List<Boolean> visited, Node current, Node des, WeightedPath path) {
		if (visited.get(current.getIndex()) && current == des) {
			return;
		} else {
			visited.set(current.getIndex(), true);
			for (Edge e : current.getEachLeavingEdge()) {
				Node two = e.getNode1();
				if (visited.get(two.getIndex())) {
					int index = path.detectNodes().indexOf(two);
					WeightedPath currentLoop = path.sublist(index, path.size());
					currentLoop.addEdge(e);
					masonResult.getLoops().add(currentLoop);
				} else {
					path.addEdge(e);
					detectLoop(graph, visited, two, des, path);
					path.removeEdge(e);
				}
			}
			visited.set(current.getIndex(), false);
		}
	}

	private boolean combination(List<List<Integer>> nontouchloops, List<Integer> comb, List<List<Boolean>> matrix,
			List<WeightedPath> loops, int r, int k, int index, double gain) {
		if (index == r) {
			delta += gain;
			nontouchloops.add(new ArrayList<>(comb));
			return true;
		} else if (k < loops.size()) {
			boolean nonTouching = true;
			boolean exist = false;
			if (index < comb.size()) {
				comb.set(index, k);
			} else {
				comb.add(k);
			}

			for (int i = 0; i < index; i++) {
				if (!matrix.get(comb.get(i)).get(k)) {
					nonTouching = false;
					break;
				}
			}
			if (nonTouching) {
				gain *= loops.get(k).getGain();
				exist = combination(nontouchloops, comb, matrix, loops, r, k + 1, index + 1, gain);
				gain /= loops.get(k).getGain();
			}
			exist |= combination(nontouchloops, comb, matrix, loops, r, k + 1, index, gain);
			return exist;
		}
		return false;

	}

	private List<List<Boolean>> setIntersectionMatrix(List<WeightedPath> loops) {
		List<List<Boolean>> matrix = new ArrayList<>();
		for (int i = 0; i < loops.size(); i++) {
			matrix.add(new ArrayList<>());
			for (int j = 0; j < i + 1; j++) {
				matrix.get(i).add(false);
			}
			for (int j = i + 1; j < loops.size(); j++) {
				matrix.get(i).add(!loops.get(i).intersects(loops.get(j)));
			}
		}
		return matrix;
	}

	private void detectNontouchingloop() {

		double totalDelta = 1;
		int sign = -1;
		List<List<Boolean>> matrix = setIntersectionMatrix(new ArrayList<>(masonResult.getLoops()));
		List<Integer> comb = new ArrayList<>();
		for (int i = 1; i <= masonResult.getLoops().size(); i++) {
			masonResult.getNontouchIndexes().add(new ArrayList<List<Integer>>());
			delta = 0;
			if (!combination(masonResult.getNontouchIndexes().get(i - 1), comb, matrix,
					new ArrayList<>(masonResult.getLoops()), i, 0, 0, 1)) {
				break;
			} else {
				totalDelta += (sign * delta);
				sign *= -1;
			}
		}
		// remove the last unnecessary pushed array
		if (masonResult.getNontouchIndexes().size() > 0
				&& masonResult.getNontouchIndexes().get(masonResult.getNontouchIndexes().size() - 1).size() == 0) {
			masonResult.getNontouchIndexes().remove(masonResult.getNontouchIndexes().size() - 1);
		}
		masonResult.getDeltas().add(totalDelta);

	}

	private List<Double> calculateDelta_is() {
		
		
		System.out.println("Inside Function");
		
		
		List<WeightedPath> arrayLoops = new ArrayList<>(masonResult.getLoops());
		WeightedPath test = arrayLoops.get(0);
		printPath(test.getPath());
		WeightedPath test2 = arrayLoops.get(1);
		printPath(test2.getPath());
		//boolean x = new ArrayList<>(masonResult.getFwPaths()).get(0).intersects();
		System.out.println(new ArrayList<>(masonResult.getFwPaths()).get(0).intersects(test));
		System.out.println(new ArrayList<>(masonResult.getFwPaths()).get(0).intersects(test2));
		System.out.println("Outside Function");
		double delta_i = 1;
		for (WeightedPath path : masonResult.getFwPaths()) {
			delta_i = 1;
			int sign = -1;
			for (int i = 0; i < masonResult.getNontouchIndexes().size(); i++) {
				double loop1 = 0;
				for (int j = 0; j < masonResult.getNontouchIndexes().get(i).size(); j++) {
					double loop2 = 1;
					for (int k = 0; k < masonResult.getNontouchIndexes().get(i).get(j).size(); k++) {
						
						WeightedPath currentLoop = arrayLoops
								.get(masonResult.getNontouchIndexes().get(i).get(j).get(k));
                       /* if (i == 2) {
                        	System.out.println("i ");
							printPath(currentLoop.getPath());
							
						}*/
						if (!path.intersects(currentLoop)) {
							loop2 *= currentLoop.getGain();
						} else {
							loop2 = 0;
							break;
						}
					}

					loop1 += loop2;

				}
				delta_i += (sign * loop1);
				sign *= -1;
			}
			masonResult.getDeltas().add(delta_i);
		}

		return masonResult.getDeltas();
	}

	private double calculateTransferFunction() {
		double valueofTF = 0;
		int index_delta_i = 1;
		for (WeightedPath path : masonResult.getFwPaths()) {
			valueofTF += (path.getGain() * masonResult.getDeltas().get(index_delta_i));
			index_delta_i++;
		}
		double bigDelta = masonResult.getDeltas().get(0);
		if (bigDelta != 0) {
			valueofTF /= bigDelta;
		} else {
			throw new RuntimeException("Division by Zero");
		}
		return valueofTF;
	}

	public Result performMasonFormula(MultiGraph graph, String srcID, String sinkID) {
		masonResult = new Result();
		calculatePathsandLoops(graph, srcID, sinkID);
		detectNontouchingloop();
		calculateDelta_is();
		masonResult.setValueOfTF(calculateTransferFunction());
		return masonResult;
	}

	public void printPath(List<Edge> path) {
		for (Edge e : path) {
			System.out.print(e.getId());
			System.out.print(" ");
		}
		System.out.print("\n");
	}

	void printLoops() {
		for (WeightedPath path : masonResult.getLoops()) {
			printPath(path.getPath());
		}
	}
}